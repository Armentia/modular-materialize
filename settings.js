module.exports = {
  httpPort: 7751,
  // Logger display levels
  // [0-n] ..(inclusive)
  //     0,     1,      2,     3,     4,     5
  // fatal, error,   warn,  info, debug, trace
  // bgRed,   red, yellow, green,  blue,  cyan
  logLevelFile: 3,
  logLevelConsole: 5,
}
