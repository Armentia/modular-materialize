

// Create a menu, add links to other pages
modular.addMenu('main');
modular.addMenuItem('main', 'Lorem', 'lorem');
modular.addMenuItem('main', 'Ipsum', 'ipsum');
modular.addMenuItem('main', 'Dolor', 'dolor');

// Load first/landing page
modular.setPage('ipsum');

for (let page of ['lorem', 'ipsum', 'dolor']) {
  modular.runFuncOnload(page, () => modular.log('joining ' + page));
  modular.runFuncOffload(page, () => modular.log('leaving ' + page));
}

//modular.addMenu('test2')
//modular.addMenuItem('test2', 'New Jersey', 'maybe')
//modular.addMenuItem('test2', 'Farmhouse Ale', 'so')
//modular.setActiveMenuNavbar('test2')
//modular.setActiveMenuNavbar('test')

// modular.runJsOnloadOnce('page2', function() {
//   document.getElementById('addTest').addEventListener('click', () => {
//     modular.addMenuItem('test', 'stringOnButton', 'boobs');
//   });
// });

// modular.runJsOnload('page1', modular.getJs('page1'));

// modular.runJsOnloadOnce(bodyContentId, function)
