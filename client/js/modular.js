const modular = initModular(socket);

var jstest;

function initModular(socket) {
  // ------------ Private-scoped ------------
  const loadingString = 'loading'; // can probably just use the contentId and save hauling a var around
  const loadingContentId = 'contentPlaceholder';

  // Internal stuff
  var menus = {};
  var menuDoms = {};
  var menuItems = [];
  var activeMenu = false;

  var bodyDoms = {[loadingContentId]: document.getElementById(loadingContentId)};
  var activeContent = loadingContentId;

  var bodyOnloads = {};
  var bodyOnloadsOnce = {};
  var bodyOffloads = {};
  var bodyOffloadsOnce = {};
  var jsLoaded = {};

  var resolves = {};

  // Requesting content from server
  //   types - body,
  function requestContent(type, id, res) {
    if (!type || !id) {
      console.log('Invalid request for content (' + type + ', ' + id + ')');
      return;
    }
    if (type === 'html') {
      if (!bodyDoms[id]) {
        bodyDoms[id] = loadingString;
        socket.emit('contentRequest', {type: type, id: id});
      } else {
        // socket.emit('contentUpdated?', {type: type, id: id});
      }
    } else if (type === 'js') {
      socket.emit('contentRequest', {type: type, id: id});
    } else {
      console.log('I don\'t know what you\'re requesting: ' + type + ', ' + id);
    }

    if (res) {
      resolves[type + '-' + id] = res;
    }
  }
  socket.on('contentResponse', response => {
    //console.log('contentResponse: ' + JSON.stringify(response));
    if (response.type === 'html') {
      // create/load dom element and keep it hidden
      bodyDoms[response.id] = document.createElement('div');
      bodyDoms[response.id].style.display = 'none';
      bodyDoms[response.id].innerHTML = response.content;
      document.getElementsByTagName('main')[0].appendChild(bodyDoms[response.id]);
    } else if (response.type === 'js') {
      jsLoaded[response.id] = response.content;
    }
    var resolveId = response.type + '-' + response.id;
    if (resolves[resolveId]) {
      resolves[resolveId]();
      resolves[resolveId] = undefined;
    }
  });
  socket.on('contentResponseFailed', response => {

  });

  function addBodyContent(bodyContentId) {
    var main = document.getElementsByTagName('main')[0];
    var newContent;

    newContent.style.display = 'none';
    main.appendChild(newContent);
  }

  function setBodyContent(pageId) {
    if (activeContent === pageId) {
      console.log('Could have sworn we were already here..');
      return;
    }
    //console.log(pageId + ' clicked');
    // Set menu item active if it refers to this page
    highlightPage(pageId);
    // Request page if we don't have it already
    if (!bodyDoms[pageId]) {
      console.log('Requesting ' + pageId);
      setBodyContent(loadingContentId);
      new Promise((resolve, reject) => {
        requestContent('html', pageId, resolve);
      }).then(result => {
        console.log(JSON.stringify(result));
        setBodyContent(pageId);
      });
      return;
    }
    // Still loading the page, set it when it's done
    if (bodyDoms[pageId] === loadingString) {
      console.log(pageId + ' is still loading.');
      // Set page to loading/placeholder until it's ready.
      setBodyContent(loadingContentId);

      new Promise(function(resolve, reject) {
        resolves['html-' + pageId] = resolve;
      }).then(function(result) {
        console.log('Body Content request resolved');
        setBodyContent(pageId); // restart the function call
      });
      return;
    }
    // Hide existing body (placeholder/loading by default)
    bodyDoms[activeContent].style.display = 'none';
    if (bodyOffloads[activeContent]) {
      for (let f of bodyOffloads[activeContent]) f();
    }
    if (bodyOffloadsOnce[activeContent]) {
      for (let f of bodyOffloadsOnce[activeContent]) f();
      bodyOffloadsOnce[activeContent] = [];
    }

    activeContent = pageId;
    // Show loading/placeholder content if we haven't preloaded the content
    document.getElementById(loadingContentId).style.display = 'inherit';
    // Show body content
    //  --onceLoaded
    document.getElementById(loadingContentId).style.display = 'none';
    bodyDoms[activeContent].style.display = 'inherit';
    // Run onload js assigned to this content
    if (bodyOnloads[activeContent]) {
      for (let f of bodyOnloads[activeContent]) f();
    }
    if (bodyOnloadsOnce[activeContent]) {
      for (let f of bodyOnloadsOnce[activeContent]) {
        f();
      }
      bodyOnloadsOnce[activeContent] = [];
    }
  };

  // Iterates through menus, adding or removing the active class as necessary.
  function highlightPage(page) {
    for (let menuId in menuDoms) {
      let menu = document.getElementById(menuId);
      for (let child of menu.children) {
        if (child.id === menu.id + '_' + page) {
          child.classList.add('active');
        } else {
          child.classList.remove('active');
        }
      }
    }
  }

  // ------------ Public-scoped ------------
  return {
    // Settings
    preloadOnMenuLink: false,

    setPage: function(pageId) {
      setBodyContent(pageId);
    },

    addMenu: function(menu) {
      menus[menu] = [];
      menuDoms[menu] = document.createElement('ul');
      menuDoms[menu].id = menu;
      menuDoms[menu].classList.add('right');
      document.getElementsByClassName('nav-wrapper')[0].appendChild(menuDoms[menu]);
      menuDoms[menu].style.display = 'none';
      // Go ahead and set the menu if this is the first one
      if (!activeMenu) {
        this.setActiveMenuNavbar(menu);
      }
    },
    addMenuItem: function(menu, menuItemText, pageId) {
      // todo optional menu item index?
      if (!menus[menu]) {
        console.log('Menu ' + menu + 'doesn\'t exist, automagically creating.');
        addMenu(menu);
      }
      var item = document.createElement('li');
      item.id = menu + '_' + pageId;
      var itemText = document.createElement('a');
      itemText.innerHTML = menuItemText;
      itemText.href = '#' + pageId;
      itemText.addEventListener('click', () => { setBodyContent(pageId); });
      item.appendChild(itemText);
      menuDoms[menu].appendChild(item);
      menuItems.push(item);
      // if pageId doesn't exist yet, request it
      if (this.preloadOnMenuLink && !bodyDoms[pageId]) requestContent('html', pageId);
      // todo ghost button until content is loaded
    },
    setActiveMenuNavbar: function(menu) { // TODO top/side?
      // Hide existing menu
      //   If first menu change, remove the placeholder
      if (!activeMenu) {
        document.getElementById('navbarPlaceholder').remove();
      } else { // Hide current menu
        document.getElementById(activeMenu).style.display = 'none';
      }
      // Show active menu
      activeMenu = menu;
      document.getElementById(activeMenu).style.display = 'inherit';
    },

    runFuncOnload: function(pageId, func) {
      if (!bodyOnloads[pageId]) {
        bodyOnloads[pageId] = [func];
      } else {
        bodyOnloads[pageId].push(func);
      }
    },
    runFuncOnloadOnce: function(pageId, func) {
      if (!bodyOnloadsOnce[pageId]) {
        bodyOnloadsOnce[pageId] = [func];
      } else {
        bodyOnloadsOnce[pageId].push(func);
      }
    },
    runFuncOffload: function(pageId, func) {
      if (!bodyOffloads[pageId]) {
        bodyOffloads[pageId] = [func];
      } else {
        bodyOffloads[pageId].push(func);
      }
    },
    runFuncOffloadOnce: function(pageId, func) {
      if (!bodyOffloadsOnce[pageId]) {
        bodyOffloadsOnce[pageId] = [func];
      } else {
        bodyOffloadsOnce[pageId].push(func);
      }
    },

    hideNavBar: function() { document.getElementsByTagName('nav')[0].style.display = 'none'; },
    showNavBar: function() { document.getElementsByTagName('nav')[0].style.display = 'block'; },
    toggleNavBar: function() {
      if (!document.getElementsByTagName('nav')[0].style.display
          || document.getElementsByTagName('nav')[0].style.display === 'block') {
        this.hideNavBar();
      } else {
        this.showNavBar();
      }
    },
    hideFooter: function() { document.getElementsByTagName('footer')[0].style.display = 'none'; },
    showFooter: function() { document.getElementsByTagName('footer')[0].style.display = 'block'; },
    toggleFooter: function() {
      if (!document.getElementsByTagName('footer')[0].style.display
          || document.getElementsByTagName('footer')[0].style.display === 'block') {
        this.hideFooter();
      } else {
        this.showFooter();
      }
    },

    // Get/load/run js stuff, usable, but not totally functional
    getJs: async function(jsId) {
      // If it's already loaded, return a function that can call it
      if (jsLoaded[jsId]) return new Function(jsLoaded[jsId]); // works
//      else return (async () => {
      await this.loadJs(jsId);
      return new Function(jsLoaded[jsId]); // doesn't work
//      })();
    },

    //        callback(new Function(jsLoaded[jsId]));
    loadJs: function(jsId) {
      if (jsLoaded[jsId]) console.log('overwriting ' + jsId);
      jsLoaded[jsId] = loadingString;

      return new Promise(function(resolve, reject) {
        requestContent('js', jsId, resolve);
      });//.then(result => {
        //resolve(jsLoaded[jsId]);
       //resolve(result);
      //});
    },
    runJs: function(jsId) {
      // Not loaded
      if (!jsLoaded[jsId]) {
        this.loadJs(jsId).then(result => {
          this.runJs(jsId); // works
        });
      // Still loading
      } else if (jsLoaded[jsId] === loadingString) {
        // todo
      // Gucci
      } else {
        new Function(jsLoaded[jsId])(); // works
      }
    },
    runJsOnload: async function(pageId, jsId) { // works
      if (!jsLoaded[jsId]) {
        await this.getJs(jsId);
      }
      this.runFuncOnload(pageId, new Function(jsLoaded[jsId]));
    },
    runJsOnloadOnce: async function(pageId, jsId) { // works
      if (!jsLoaded[jsId]) {
        await this.getJs(jsId);
      }
      this.runFuncOnloadOnce(pageId, new Function(jsLoaded[jsId]));
    },

    // Logging
    log: function(message) {
      console.log(message);
      M.toast({html: message});
    },

    // Debug stuffs
    _req: function(type, id) {
      requestContent(type, id);
    }
  };
}
