const socket = io();
const disconnectRefreshTime = 60 * 1000;

socket.on('connection', socket => {
  console.log('Connected to server');
});

socket.on('version', version => {
  socket.version = version;
});

// Check how long it's been since we've been connected
// Automagicaly refresh after a set amount of disconnect time
socket.on('disconnect', (reason) => {
  console.log('Disconnected: ' + reason);
  socket.lastConnected = new Date();
});
socket.on('reconnect', (attemptNum) => {
  if (socket.lastConnected) {
    var timeSince = new Date() - socket.lastConnected;
    if ((timeSince < disconnectRefreshTime) && socket.version) {
      console.log('Reconnected after ' + (timeSince/1000) + ' seconds');
      // Send our version out to the server
      socket.emit('version', socket.version);
      return;
    }
  }
  console.log('too long or no version');
  socket.disconnect();
  location.reload();
});

// Not sure the actual difference between reconnect_attempt and reconnecting.
// Both happen regardless of pass or fail. They happen in that order. ^
// socket.on('reconnect_attempt', (attemptNum) => {});
// socket.on('reconnecting', (attemptNum) => {});

// If we tell the server we have a version that doesn't match,
// the server will respond with an expiredVersion event, then
// let's update via refresh.
socket.on('expiredVersion', version => {
  socket.disconnect();
  location.reload();
});
// reload vs refresh, any difference?

socket.on('reload', () => {
  socket.disconnect();
  location.reload();
});
