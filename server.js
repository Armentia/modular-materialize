const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const term = require('terminal-kit').terminal;

var settings = require('./settings');
var logger = require('./server/logger.js')({terminal: term, io: io});
var modular = require('./server/modular.js')({logger: logger});

var connectionCount = 0;
var revokeRoot;

// Configuration checks
if (settings.httpPort < 1000) { // requires root
  var uid = parseInt(process.env.SUDO_UID);
  if (uid) { // we're root
    revokeRoot = uid;
  } else {
    console.log('Using ports under 1000 requires root access. settings.httpPort === ' + settings.httpPort);
    process.exit(1);
  }
}

// Initialize some logger stats
logger.addStat('connections', 'Connections', 0);

// Statically serve up files from client folder
app.use('/', express.static('client'));

// Configure the socket connection
io.on('connection', socket => {
  logger.debug(socket.handshake.address + ': Client connected');
  connectionCount++; logger.setStat('connections', connectionCount);
  socket.on('disconnect', () => {
    logger.debug(socket.handshake.address + ': Client disconnected');
    connectionCount--; logger.setStat('connections', connectionCount);
  });

  // Initialize socket connection for modular
  modular.initSocket(socket);
});

http.listen(settings.httpPort);
if (revokeRoot) process.setuid(uid);

// Take over input
term.grabInput({mouse: 'button'});
term.on('key', (name, matches, data) => {
  if (name === 'CTRL_C') {
    quitProgram();
  } else if (name === 'ENTER') {
    logger.refresh();
  } else {
    logger.type(name);
  }
});

function quitProgram() {
  logger.info('Exiting...');
  term.grabInput(false);
  setTimeout(() => process.exit(), 100); // don't remember why I added a delay before exiting.
}

logger.setHeader('Modular Materialize - Listening on port ' + settings.httpPort);
logger.fatal('walrus pancakes');
logger.error('oh the huge manatee');
logger.warn('Boxing is just show business with blood. -Frank Bruno');
logger.info('seemsgood');
logger.debug('coffee pots');
logger.trace('not enough coffee');
