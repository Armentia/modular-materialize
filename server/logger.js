// goals
// - split low level and high level (e.g. string manipulation vs terminal display)

// stats
// - active connections, total connections, total unique connections

// https://en.wikipedia.org/wiki/Box-drawing_character
// bordersets [vertical, horizontal, ne, nw, sw, se, !n, !e, !s, !w, center]
// ║═╚╝╗╔╦╣╩╠╬
// bordersets [vertical, horizontal, nw, n, ne, e, se, s, sw, w, center]
// ║═╔╦╗╣╝╩╚╠╬ double
// │─┌┬┐┤┘┴└├┼ single
// ┃━┏┳┓┫┛┻┗┣╋ thick

// imports
const fs = require('fs');
const settings = require('../settings');

// Important stuffs
var term, io;
var logFile = './logs/modmat-' + new Date().toISOString() + '.log'; // todo pull path from settings
if (!fs.existsSync('./logs')) fs.mkdirSync('./logs'); // make log directory if it doesn't exist
var logStream = fs.createWriteStream(logFile, {flags: 'a'});

// Logger stuffs
const maxMessages = 80; // todo store just more than can be displayed on a 1080 * numTypes
var messages = new Array(maxMessages);
const levels = ['fatal', 'error', ' warn', ' info', 'debug', 'trace'];

// Information that can be displayed
var header = 'Modular Materialize';
var stats = {};
var termHeight = 42;

// user-typed input
var typed = '';

// Log stuffs
function log(type, message) {
  var now = new Date();
  messages.push([type, now, message]);
  if (messages.length >= maxMessages) messages.shift(); // todo pushing and shifting to this probably has some drawbacks..
  updateTerm();
  // log to file
  if (type <= settings.logLevelFile)
    logStream.write(now.toISOString() + ' ' + message + '\n');
}

function updateHeader() {
  term.clear();
  term.moveTo(1,1);

  let lines = 1; // Number of header lines printed

  // Top bar
  term(formatLine(header, {width: term.width, left: '╔', right: '╗', fill: '═', align: 'center'}));

  // Various statistics
  for (let stat of Object.keys(stats)) {
    term.cyan(formatLine(stats[stat].description + ': ' + stats[stat].value,
                         {width: term.width, edge: '║'}));
    lines++;
  }
  // Blank lines
  let blankLine = formatLine('', {width: term.width, edge: '║'});
  for (let i = lines; i <= Math.ceil(termHeight/2); i++) { // todo going too far?
    term(blankLine);
  }

  updateLog();
}

// Display stuffs
function updateTerm() {
  termHeight = term.height; updateHeader(); return;
  if (termHeight !== term.height) {
    termHeight = term.height;
    updateHeader();
    //updateLog();
  } else {
    updateLog();
  }
}

function updateLog() {
  // Display log on bottom half
  let logLines = Math.floor(termHeight/2);
  term.moveTo(1, logLines);
  term(formatLine('Log', {width: term.width, left: '╠', right: '╣', fill: '═', align: 'center'}));

  var line = termHeight-2; // start from end of terminal
  var i = maxMessages-1; // start from end of log
  term.moveTo(1, line);
  while (i >= 0) {// maxMessages) {
    if (!messages[i]) { i--; continue; } // log doesn't go back far enough, short circuit
    if (messages[i][0] <= settings.logLevelConsole) {
      term.moveTo(1, line);
      let level = messages[i][0];
      var text = formatLine(levels[level] + '-'
                        + formatDate(messages[i][1])
                        + ' ' + messages[i][2], {width: term.width, edge: '║'});
      if (level === 0) term.bgRed(text); // fatal
      else if (level === 1) term.red(text); // error
      else if (level === 2) term.yellow(text); // warn
      else if (level === 3) term.green(text); // info
      else if (level === 4) term.cyan(text); // debug
      else term.blue(text); // trace
      line--;
    }
    if (line <= Math.floor(termHeight/2)) break; // don't overwrite top section eh?
    i--;
  }
  // write empty lines until we hit that threshold
  let blankLine; // only bother defining if we actually use it.
  while (line > Math.floor(termHeight/2)) {
    if (!blankLine) blankLine = formatLine('', {width: term.width, edge: '║'});
    term.moveTo(1, line);
    term(blankLine);
    line--;
  }

  // Draw the bottom border, and in turn put the text cursor below the display
  term.moveTo(1, termHeight-1);
  term(formatLine('', {width: term.width, left: '╚', right: '╝', fill: '═'}));
  term(typed);
}

// To be replaced by String.prototype.padStart/padEnd, built-in JS in some future version of nodejs
String.prototype.padLeft = function(padding) {
  return String(padding + this).slice(-padding.length);
}
String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
};
function formatDate(date) {
  return "{0}:{1}:{2}".format(date.getHours().toString().padLeft('00'),
                              date.getMinutes().toString().padLeft('00'),
                              date.getSeconds().toString().padLeft('00'));
}

// Options:
// - required width // todo error check
// - optional [decorations] left, right, fill, edge
// - optional [other] align, padding, noNewLine
// - optional notruncate
function formatLine(text, options) {
  var output = '';
  if (options.left) output += options.left;
  else if (options.edge) output += options.edge;

  if (options.align === 'center') {
    output += new Array(Math.round((options.width - text.length)/2) - output.length)
      .join(options.fill || ' ');
    if (!options.padding) { // copypasta, possible cleanup?
      if (options.left || options.edge) options.padding = 1;
      else options.padding = 0;
    }
    if (options.padding >= 0) output += new Array(options.padding + 1).join(' ');
    output += text;
    if (options.padding >= 0) output += new Array(options.padding + 1).join(' ');
    if (options.right || options.edge) {
      if (options.width > output.length)
        output += new Array(options.width - output.length).join(options.fill || ' ');
    }
  } else if (options.align === 'right') { // todo
  } else { // options.align === 'left'
    if (!options.padding) {
      if (options.left || options.edge) options.padding = 1;
      else options.padding = 0;
    }
    if (options.padding >= 0 && text !== '') output += new Array(options.padding + 1).join(' ');
    output += text;
    // truncate to line length
    if (!options.notruncate) {
      if (output.length > options.width) output = output.substring(0, options.width-3) + '…';
    }
    if (options.right || options.edge) {
      if (options.width > output.length)
        output += new Array(options.width - output.length).join(options.fill || ' ');
    }
  }

  if (options.right) output += options.right;
  else if (options.edge) output += options.edge;

  if (!options.noNewLine) output += '\n';
  // output
  return output;
}

function checkCommand(text) {
  // initial cleanup
  text = text.trim(); text = text.toLowerCase();
  if (!text) return;
  let split = text.split(' ');
  let cmd = split[0];
  let arg1;
  if (split.length > 1) arg1 = split[1];
  if (cmd === 'help') {
    if (arg1) {
      if (arg1 === 'log') {
        log(3, 'Sets the log display level. Valid arguments are fatal, error, warn, info, debug, and trace. Also accepted is numeric input that corresponds to the previously mentioned levels, 0 through 5.');
      } else if (arg1 === 'reset') {
        log(3, 'Causes all connected clients to reload');
      } else {
        log(3, 'There\'s no help for you.');
      }
    } else {
      log(3, 'Help - commands available: log, reset');
    }
  } else if (cmd === 'log') {
    if (arg1) {
      if (Number.parseInt(arg1)) {
        settings.logLevelConsole = Number.parseInt(arg1);
      } else {
        if (arg1 === 'fatal') settings.logLevelConsole = 0;
        else if (arg1 === 'error') settings.logLevelConsole = 1;
        else if (arg1 === 'warn') settings.logLevelConsole = 2;
        else if (arg1 === 'info') settings.logLevelConsole = 3;
        else if (arg1 === 'debug') settings.logLevelConsole = 4;
        else if (arg1 === 'trace') settings.logLevelConsole = 5;
        else log(1, 'unknown log level ' + arg1);
      }
    } else {
      log(2, 'set log level - please include a numeric or text argument');
    }
  } else if (cmd === 'reset') {
    log(3, 'Asking all clients to reload their pages and reconnect');
    io.emit('reload');
  } else {
    log(1, 'Unknown command ' + text);
  }
}

module.exports = config => {
  term = config.terminal;
  io = config.io;
  this.fatal = message => log(0, message);
  this.error = message => log(1, message);
  this.warn = message => log(2, message);
  this.info = message => log(3, message);
  this.debug = message => log(4, message);
  this.trace = message => log(5, message);

  // Stuffs
  this.setHeader = text => {
    header = text;
    updateHeader();
  };

  // Stat tracking
  this.addStat = (name, desc, defaultValue, priority) => {
    stats[name] = {
      description: desc,
      defaultValue: defaultValue,
      value: defaultValue,
      priority: priority || 100
    };
  };
  this.setStat = (name, value) => {
    if (!stats[name]) {
      logger.fatal('no stat named ' + name); exit(1);
    }
    stats[name].value = value;
    updateHeader();
  };
  this.getStat = name => {
    if (stats[name]) return stats[name].value;
  };
  this.modStat = (name, mod) => { // error checking
    stats[name].value += mod;
    updateHeader();
  }

  this.updateConnectionCount = count => {
    connectionCount = count;
    updateHeader();
  };

  this.refresh = () => {
    if (typed) {
      checkCommand(typed);
      typed = '';
    }
    updateTerm();
  }

  // input
  this.type = text => {
    if (text === 'BACKSPACE') typed = typed.substr(0, typed.length - 1);
    else typed += text;
    updateTerm();
  }
  return this;
}
