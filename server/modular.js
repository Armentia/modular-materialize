const fs = require('fs')

var logger;

// todo
// Loading media (images, videos,
// finish js loading
// requesting stuff from within loaded html requests (requires at start of file)

module.exports = config => {
  if (config && config.logger) {
    logger = config.logger
  } else {
    var log = msg => { console.log(msg); };
    logger = { fatal: log, error: log, warn: log, info: log, debug: log, trace: log };
  }

  this.initSocket = socket => {
    socket.emit('version', process.env.npm_package_version);
    socket.on('contentRequest', req => {
      if (!req.type || !req.id) {
        logger.error('invalid content request: ' + req.type + ', ' + req.id);
        return;
      }

      var prefix = '/../client/';
      var postfix = '.html';

      // determine what's being requested
      if (req.type === 'html') {
      } else if (req.type === 'js') {
        prefix += 'js/';
        postfix = '.js';
      } else {
        logger.error('Unknown contentResponseType: ' + req.type);
        req.error = 'Unrecognized type request';
        socket.emit('contentRequestFailed', req);
        return;
      }

      var res = {};

      // read in file
      var filePath = __dirname + prefix + req.id + postfix;
      fs.readFile(filePath, 'utf8', (err, content) => {
        if (content) {
          logger.info('Successfully read in ' + req.id + '.' + req.type);
          res.id = req.id;
          res.type = req.type;
          res.content = content;
          // push back out to client
          socket.emit('contentResponse', res);
        } else {
          logger.error('No ' + req.type + ' file for ' + req.id + ' at ' + filePath);
          req.error = 'Content does not exist';
          socket.emit('contentRequestFailed', req);
        }
      });
    });

    // Client version control
    // todo send version on client connect
    // respond to client stating their version
    socket.on('version', version => {
      logger.info('Received version ' + version + ' from client.');
      var serverVersion = process.env.npm_package_version || 'did not start server with npm start you idget'; //todo be less drank
      if (version !== serverVersion) {
        socket.emit('expiredVersion', serverVersion);
      }
    });
  };

  return this;
}
